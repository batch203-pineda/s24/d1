// ES6 is also know as ECMAScript 2015
// ECMAScript is the standard that is used to create implementation of the language, one which is JavaScript.
// where all browser vendor can implement (Apple, Google, Microsoft, Mozilla, etc.)
// New features to JavaScript



//  [SECTION] Exponent Operator

// Using Math object methods

const firstNum = Math.pow(8, 2);
console.log(firstNum);

// Using the exponent operator
const secondNum = 8 ** 2;
console.log(secondNum);










// [SECTION] Template Literals

/* 
    - Allows to write string without using the concatenation operator (+).
    - ${} is called placeholder when using template literals, and we can input variables or
 */

let name = "John";

// Hello John! Welcome to Programming!

// Pre-Template Literals
let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals:");
console.log(message);


// Strings using template literals
// Uses backticks(``) instead of ("") or ('')
message = `Hello ${name}! Welcome to programming!`;
console.log("Message with template literals:");
console.log(message);

const anotherMessage = `${name} attended a math competition.
He won it by solving the problem "8 ** 2" with the solutionof ${8**2}`;
console.log(anotherMessage);









//  [SECTION]   Array Destructuring
//  It allows us to name array elements with variableNames instead of using the index numbers.

/* 
    Syntax:
        let/const [variableName1, variableName2, variableName3 ] = arrayName;
*/

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// Hello Juan Dela Cruz! It's nice to meet you
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

// Array Destructuring
    // variable naming for array destructuring is based on the developers choice.
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);









//  [SECTION]   Object Destructuring

/* 
    - Shortens the syntax for acessing properties from object.
    - The difference with array destructuring, this should be exact property name.

    Syntax:
        let/const {propertyNameA, propertyNameB, propertyNameC} = object;
*/

const person = {
    givenName: "Jane",
    maidenName: "Dela",
    surName: "Cruz"
}

    // Pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.lastName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.lastName}! It's good to see you again.`);

    // Object Destructuring
/* const {givenName, maidenName, surName} = person;
console.log(givenName);
console.log(maidenName);
console.log(surName);

console.log(`Hello ${givenName} ${maidenName} ${surName}! It's good to see you again.`); */

// Object destructuring can also be done inside the parameter.
function getFullName({givenName, maidenName, surName}) {
    console.log(`Hello ${givenName} ${maidenName} ${surName}!`);
}
getFullName(person);







//  [SECTION] Arrow Functions
/* 
    - Compact alternative syntax to traditional functions.
    - Usefull for code snippets where creating functions will not be reused in any other portion of the code.
    - This will only work with function expression.
    - "DRY (Don't Repeat Yourself)" principle

    Syntax:

        lets/const variableName = () => {
            // code block/statement
        }

        or

        lets/const variableName = (parameter) => {
            // code block/statement with parameter
        }

*/
      
        // fuction declaration
        // function hello() {
        //     console.log("Hello World!");
        // }

        // function expression. bawal i-invoke before initialization.
        let hello = () => {
            console.log("This is sample Arrow function");
        }
        hello();


        const students = ["John", "Jane", "Judy"];

        // Arrow functions using Array Iteration method
        // Pre-Arrow Function

        // students.forEach(function (student) {
        //     console.log(`${student} is a student`);
        // })

        // Arrow Function
        students.forEach((student) => {
            console.log(`${student} is a student.`);
        });








//  [SECTION] Implicit Return Statement
//  There are instances where you can omit the "return" statement.

// const add = (x,y) => {
//     return x + y;
// }

// Using implicit return
const add = (x,y) => x + y;

let total = add(1, 2);
console.log(total);


const numbers = [1, 2, 3, 4, 5];
const squareValues = numbers.map((numbers) => numbers**2);
console.log(squareValues);










//  [SECTION] Default Function Argument Value
//  Provides a default argument value if none is provided when the function is invoked.

// const greet = (name) => `Good morning, ${name}`;
// console.log(greet()); // no argument provided will result to undefined

const greet = (name = "User") => `Good morning, ${name}`;
console.log(greet());
console.log(greet("Love"));







//  [SECTION] Class-Based Object Blueprint
//  Allows creation/instantiation of object using classes as blueprints.
/* 
        - The "constructor" is a special method of a class for creating/initializing object for that class.

        Syntax:

            class className{
                constructor(objectPropertyA, objectPropertyB){
                this.objectPropertyA = objPropertyA;
                this.objectPropertyB = objPropertyB;
            }
            // insert function outside our constructor
        }
*/  
        // Function constructor
        // function car(brand, name, year){
        //     this.brand = brand;
        //     this.name = name;
        //     this.year = year;
        // }

        class car {
            constructor(brand, name, year){
                this.brand = brand;
                this.name = name;
                this.year = year;
            }
        }

        // "new" operator creates/instatiates a new object with the given argument as value of it's property
        const myCar = new car();
        console.log(myCar); // we have created a object but it's property is undefined.

        // Reassign value of each properties
        myCar.brand = "Ford";
        myCar.name = "Ranger Raptor";
        myCar.year = 2021;

        console.log(myCar);

